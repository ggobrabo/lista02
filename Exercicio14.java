import java.util.Scanner;

public class Exercicio14 {
    public static void executar() {
        Scanner scanner = new Scanner(System.in);

      
        int[] vetor = new int[5];

        System.out.println("Digite 5 números:");
        for (int i = 0; i < vetor.length; i++) {
            vetor[i] = scanner.nextInt();
        }

       
        int indiceMaior = 0;
        for (int i = 1; i < vetor.length; i++) {
            if (vetor[i] > vetor[indiceMaior]) {
                indiceMaior = i;
            }
        }

        
        if (indiceMaior != vetor.length - 1) {
            int temp = vetor[indiceMaior];
            vetor[indiceMaior] = vetor[vetor.length - 1];
            vetor[vetor.length - 1] = temp;
        }

     
        System.out.println("Vetor atualizado:");
        for (int valor : vetor) {
            System.out.print(valor + " ");
        }

        scanner.close();
    }
}
