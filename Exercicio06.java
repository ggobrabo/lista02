import java.util.Scanner;

public class Exercicio06 {
    public static void executar() {
        Scanner scanner = new Scanner(System.in);

       
        double[] notas = new double[5];
        double[] pesos = new double[5];

        System.out.println("Digite as notas do aluno:");
        for (int i = 0; i < notas.length; i++) {
            notas[i] = scanner.nextDouble();
        }

        System.out.println("Digite os pesos correspondentes:");
        for (int i = 0; i < pesos.length; i++) {
            pesos[i] = scanner.nextDouble();
        }

        
        double somaProdutos = 0;
        double somaPesos = 0;
        for (int i = 0; i < notas.length; i++) {
            somaProdutos += notas[i] * pesos[i];
            somaPesos += pesos[i];
        }

        double mediaPonderada = somaProdutos / somaPesos;

        System.out.println("A média ponderada do aluno é: " + mediaPonderada);

        scanner.close();
    }
}
