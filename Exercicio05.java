import java.util.Scanner;

public class Exercicio05 {
    public static void executar() {
        Scanner scanner = new Scanner(System.in);

       
        int[] vetor = new int[5];
        System.out.println("Digite 5 números:");
        for (int i = 0; i < vetor.length; i++) {
            vetor[i] = scanner.nextInt();
        }

      
        System.out.print("Digite o número n: ");
        int n = scanner.nextInt();

   
        int contador = 0;
        for (int valor : vetor) {
            if (valor == n) {
                contador++;
            }
        }

        System.out.println("O número " + n + " aparece " + contador + " vezes no vetor.");

        scanner.close();
    }
}
