import java.util.Scanner;

public class Exercicio09 {
    public static void executar() {
        Scanner scanner = new Scanner(System.in);

        
        int[] vetorA = new int[5];
        int[] vetorB = new int[5];

        System.out.println("Digite os elementos do vetor A:");
        for (int i = 0; i < vetorA.length; i++) {
            vetorA[i] = scanner.nextInt();
        }

        System.out.println("Digite os elementos do vetor B:");
        for (int i = 0; i < vetorB.length; i++) {
            vetorB[i] = scanner.nextInt();
        }

        
        int[] vetorC = new int[5];
        for (int i = 0; i < vetorA.length; i++) {
            if (i % 2 == 0) { 
                vetorC[i] = vetorA[i];
            } else { 
                vetorC[i] = vetorB[i];
            }
        }

      
        System.out.println("O vetor C gerado é:");
        for (int valor : vetorC) {
            System.out.print(valor + " ");
        }

        scanner.close();
    }
}
