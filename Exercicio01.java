import java.util.Scanner;

public class Exercicio01 {
    public static void executar() {
        Scanner scanner = new Scanner(System.in);

        
        int[] vetor = new int[5];
        double soma = 0;

        System.out.println("Digite 5 números:");
        for (int i = 0; i < vetor.length; i++) {
            vetor[i] = scanner.nextInt();
            soma += vetor[i];
        }

        
        double media = soma / vetor.length;

        
        System.out.println("Valores menores que a média:");
        for (int valor : vetor) {
            if (valor < media) {
                System.out.println(valor);
            }
        }

        System.out.println("Valores iguais à média:");
        for (int valor : vetor) {
            if (valor == media) {
                System.out.println(valor);
            }
        }

        System.out.println("Valores superiores à média:");
        for (int valor : vetor) {
            if (valor > media) {
                System.out.println(valor);
            }
        }

        scanner.close();
    }
}
