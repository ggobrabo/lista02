import java.util.Scanner;

public class Exercicio13 {
    public static void executar() {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Digite o valor de n: ");
        int n = scanner.nextInt();

        
        int[] vetorFibonacci = new int[n];

       
        for (int i = 0; i < n; i++) {
            if (i == 0 || i == 1) {
                vetorFibonacci[i] = 1;
            } else {
                vetorFibonacci[i] = vetorFibonacci[i - 1] + vetorFibonacci[i - 2];
            }
        }

        
        System.out.println("Os " + n + " primeiros termos da sequência de Fibonacci são:");
        for (int valor : vetorFibonacci) {
            System.out.print(valor + " ");
        }

        scanner.close();
    }
}
