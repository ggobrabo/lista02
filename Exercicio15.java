import java.util.Scanner;

public class Exercicio15 {
    public static void executar() {
        Scanner scanner = new Scanner(System.in);

       
        int[] vetor = new int[5];

        System.out.println("Digite 5 números:");
        vetor[0] = scanner.nextInt(); 

       
        int ultimoNumero = vetor[0];
        for (int i = 1; i < vetor.length; i++) {
            int numero = scanner.nextInt();
            if (numero > ultimoNumero) {
                vetor[i] = numero;
                ultimoNumero = numero;
            } else {
                System.out.println("O número digitado não é maior que o anterior. Digite outro número:");
                i--; 
            }
        }

 
        System.out.println("O vetor preenchido de acordo com a regra é:");
        for (int valor : vetor) {
            System.out.print(valor + " ");
        }

        scanner.close();
    }
}
