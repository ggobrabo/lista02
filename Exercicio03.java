import java.util.Scanner;

public class Exercicio03 {
    public static void executar() {
        Scanner scanner = new Scanner(System.in);

        
        System.out.print("Digite o tamanho do vetor: ");
        int tamanho = scanner.nextInt();
        int[] vetor = new int[tamanho];

        System.out.println("Digite os números:");
        for (int i = 0; i < vetor.length; i++) {
            vetor[i] = scanner.nextInt();
        }

        
        int[] vetorDobro = new int[tamanho];
        for (int i = 0; i < vetor.length; i++) {
            vetorDobro[i] = vetor[i] * 2;
        }

        
        System.out.println("Valores do segundo vetor (dobro):");
        for (int valor : vetorDobro) {
            System.out.println(valor);
        }

        scanner.close();
    }
}
