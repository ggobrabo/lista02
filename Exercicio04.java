import java.util.Scanner;

public class Exercicio04 {
    public static void executar() {
        Scanner scanner = new Scanner(System.in);

      
        int[] vetor = new int[5];
        System.out.println("Digite 5 números:");
        for (int i = 0; i < vetor.length; i++) {
            vetor[i] = scanner.nextInt();
        }

   
        System.out.print("Digite o valor de n: ");
        int n = scanner.nextInt();


        System.out.println("Índices dos elementos inferiores a " + n + ":");
        for (int i = 0; i < vetor.length; i++) {
            if (vetor[i] < n) {
                System.out.println("Índice: " + i + ", Valor: " + vetor[i]);
            }
        }

        scanner.close();
    }
}
