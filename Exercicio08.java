import java.util.Scanner;

public class Exercicio08{
    public static void executar() {
        Scanner scanner = new Scanner(System.in);

        
        int[] vetorA = new int[5];
        int[] vetorB = new int[5];

        System.out.println("Digite os elementos do vetor A:");
        for (int i = 0; i < vetorA.length; i++) {
            vetorA[i] = scanner.nextInt();
        }

        System.out.println("Digite os elementos do vetor B:");
        for (int i = 0; i < vetorB.length; i++) {
            vetorB[i] = scanner.nextInt();
        }

       
        int[] vetorC = new int[vetorA.length + vetorB.length];
        System.arraycopy(vetorA, 0, vetorC, 0, vetorA.length);
        System.arraycopy(vetorB, 0, vetorC, vetorA.length, vetorB.length);

      
        System.out.println("O vetor C (concatenação de A e B) é:");
        for (int valor : vetorC) {
            System.out.print(valor + " ");
        }

        scanner.close();
    }
}
